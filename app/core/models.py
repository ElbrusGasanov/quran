from django.db import models


class Surah(models.Model):
    number = models.IntegerField()
    name = models.CharField(max_length=64)

    def __str__(self) -> str:
        return f"{self.number} - {self.name}"


class Ayat(models.Model):
    surah = models.ForeignKey(
        to=Surah,
        on_delete=models.CASCADE,
    )
    number = models.IntegerField()
    text = models.TextField()

    def __str__(self) -> str:
        return self.text
